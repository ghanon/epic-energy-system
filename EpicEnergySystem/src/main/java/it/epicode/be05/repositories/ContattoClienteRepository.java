package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.epicode.be05.model.ContattoCliente;

public interface ContattoClienteRepository extends JpaRepository<ContattoCliente, Long> {

}
