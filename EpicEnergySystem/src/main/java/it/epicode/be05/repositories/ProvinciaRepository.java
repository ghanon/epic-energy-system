package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.epicode.be05.model.Provincia;

public interface ProvinciaRepository extends JpaRepository<Provincia, Long> {

	Provincia findByNome (String p);
	
}
