package it.epicode.be05.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.epicode.be05.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	
	//---METODI DI ORDINAMENTO	


	
	@Query ("SELECT c FROM Cliente c ORDER BY c.fatturatoAnnuale")
	Page<Cliente> orderByFatturatoAnnuale (Pageable p);
	
	

	@Query ("SELECT c FROM Cliente c ORDER BY c.dataInserimento")
	Page<Cliente> orderByDataInserimento(Pageable p);
	
	
	
	@Query ("SELECT c FROM Cliente c ORDER BY c.dataUltimoContatto")
	Page<Cliente> orderByDataUltimoContatto(Pageable p);
	
	
	
	@Query ("SELECT c FROM Cliente c ORDER BY c.indirizzoSedeLegale.comune.provincia.sigla")
	Page<Cliente> orderByProvincia(Pageable p);
	
	
	
	//---METODI DI FILTRAGGIO
	
	Page<Cliente> findAll (Pageable p);
	
	
	Page<Cliente> findByRagioneSociale (String nome, Pageable p);
	
	
	Page<Cliente> findByFatturatoAnnuale (Long FattAnn,Pageable p);
	
	
	Page<Cliente> findByDataInserimento(LocalDate d,Pageable p);
	
	
	Page<Cliente> findByDataUltimoContatto(LocalDate d,Pageable p);
	
	
}
