package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.epicode.be05.model.Comune;

public interface ComuneRepository extends JpaRepository<Comune, Long> {

}
