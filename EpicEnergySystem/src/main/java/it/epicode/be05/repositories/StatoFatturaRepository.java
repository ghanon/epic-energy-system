package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.epicode.be05.model.StatoFattura;

public interface StatoFatturaRepository extends JpaRepository<StatoFattura, Long> {

}
