package it.epicode.be05.repositories;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;

public interface FatturaRepository extends JpaRepository<Fattura, Long> {
	

	Page<Fattura> findByCliente (Cliente cliente, Pageable p);
	Page<Fattura> findByStatoFattura (StatoFattura statoFattura, Pageable p);
	Page<Fattura> findByData (Date data, Pageable p);
	Page<Fattura> findByAnno (int anno, Pageable p);
	Page<Fattura> findByImportoBetween (BigDecimal importoMin, BigDecimal importoMax, Pageable p);

}
