package it.epicode.be05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpicEnergySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpicEnergySystemApplication.class, args);
	}

}
