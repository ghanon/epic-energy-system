package it.epicode.be05.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name="ContattoCliente")
@Builder
@AllArgsConstructor

public class ContattoCliente {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String nomeContatto;
	@Column(nullable = false, length = 50)
	private String cognomeContatto;
	@Column(nullable = false, length = 50)
	private String emailContatto;
	@Column(nullable = false, length = 50)
	private String telefonoContatto;
}
