package it.epicode.be05.model;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name="Fattura")
@Builder
@AllArgsConstructor
public class Fattura {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(nullable = false, length = 50)
	private int anno;
	private Date data;
	@Column(nullable = false, length = 50)
	private BigDecimal importo;
	private int numero;

	@OneToOne
	private StatoFattura statoFattura;

	@ManyToOne 
	private Cliente cliente;
}
