package it.epicode.be05.model;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name="Cliente")
@AllArgsConstructor
@Builder
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(nullable = false, length = 50)
	private String ragioneSociale;
	@Column(nullable = false, length = 50)
	private String partitaIva;
	private String email;
	private LocalDate dataInserimento;
	private LocalDate dataUltimoContatto;
	private long fatturatoAnnuale;
	@Column(nullable = false, length = 50)
	private String pec;
	@Column(nullable = false, length = 50)
	private long telefono;
	
	@OneToOne
	private ContattoCliente contattoCliente;

	@OneToOne
	private Indirizzo indirizzoSedeLegale;
	@OneToOne
	private Indirizzo indirizzoSedeOperativa;
	
	
	private TipoCliente tipo;
	
	@OneToMany 
	private List<Fattura> fattura;
}
