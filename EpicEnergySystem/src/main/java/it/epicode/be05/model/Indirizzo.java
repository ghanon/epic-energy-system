package it.epicode.be05.model;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name="Indirizzo")
@Builder
@AllArgsConstructor
public class Indirizzo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(nullable = false, length = 50)
	private String via;
	@Column(nullable = false, length = 50)
	private String civico;
	@Column(nullable = false, length = 50)
	private String località;
	private int cap;
	
	@ManyToOne 
	private Comune comune; 

}
