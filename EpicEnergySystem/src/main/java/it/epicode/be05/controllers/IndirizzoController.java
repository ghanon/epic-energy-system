package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.epicode.be05.model.Indirizzo;
import it.epicode.be05.services.IndirizzoService;

@RestController
@RequestMapping("/api/indirizzo")
public class IndirizzoController {

    @Autowired
    IndirizzoService service;


    @PostMapping("/save")
    public ResponseEntity<Indirizzo> saveIndirizzo(@RequestBody Indirizzo indirizzo){
        service.create(indirizzo);
        return new ResponseEntity<Indirizzo>(indirizzo, HttpStatus.ACCEPTED);
    }

    @GetMapping("/delete/{id}")
    public void deleteIndirizzo (long id) {
        service.delete(id);
    }


    @GetMapping("/search/{id}")
    public ResponseEntity<Indirizzo> searchIndirizzo (long id ){
        return new ResponseEntity<Indirizzo>(service.readById(id), HttpStatus.OK);
    }

    @PostMapping("/modify")
    public ResponseEntity<Indirizzo> modifyIndirizzo (@RequestBody Indirizzo indirizzo, long id){
        service.update(id, indirizzo);
        return new ResponseEntity<Indirizzo>(indirizzo, HttpStatus.ACCEPTED);
    }

}