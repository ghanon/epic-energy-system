package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Provincia;
import it.epicode.be05.services.ProvinciaService;

@RestController
@RequestMapping("/api/provincia")
public class ProvinciaController {

	@Autowired
	ProvinciaService service;
	
	@PostMapping("/save")
	public ResponseEntity<Provincia> saveProvincia(@RequestBody Provincia provincia){
		service.create(provincia);
		return new ResponseEntity<Provincia>(provincia, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteProvincia (long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Provincia> searchProvincia (long id ){
		return new ResponseEntity<Provincia>(service.readById(id), HttpStatus.OK);
	}
	
	@PostMapping("/modify")
	public ResponseEntity<Provincia> modifyProvincia (@RequestBody Provincia provincia, long id){
		service.update(id, provincia);
		return new ResponseEntity<Provincia>(provincia, HttpStatus.ACCEPTED);
	}
	
	
	
}
