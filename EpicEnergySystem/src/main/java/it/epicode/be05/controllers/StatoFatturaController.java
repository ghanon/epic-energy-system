package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.services.StatoFatturaService;

@RestController
@RequestMapping("/api/statoFattura")
public class StatoFatturaController {

	@Autowired
	StatoFatturaService service;
	
	@PostMapping("/save")
	public ResponseEntity<StatoFattura> saveStatoFattura(@RequestBody StatoFattura stato){
		service.create(stato);
		return new ResponseEntity<StatoFattura>(stato, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteStatoFattura (long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<StatoFattura> searchStatoFattura (long id ){
		return new ResponseEntity<StatoFattura>(service.readById(id), HttpStatus.OK);
	}
	
	@PostMapping("/modify")
	public ResponseEntity<StatoFattura> modifyStatoFattura (@RequestBody StatoFattura stato, long id){
		service.update(id, stato);
		return new ResponseEntity<StatoFattura>(stato, HttpStatus.ACCEPTED);
	}
	
	
	
	
	
	
	
}
