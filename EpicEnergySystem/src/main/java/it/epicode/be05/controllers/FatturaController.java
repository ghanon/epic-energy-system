package it.epicode.be05.controllers;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.services.FatturaService;

@RestController
@RequestMapping("/api/fattura")
public class FatturaController {

    @Autowired
    FatturaService service;


    @PostMapping("/save")
    public ResponseEntity<Fattura> saveFattura(@RequestBody Fattura fattura){
        service.create(fattura);
        return new ResponseEntity<Fattura>(fattura, HttpStatus.ACCEPTED);
    }

    @GetMapping("/delete/{id}")
    public void deleteFattura (long id) {
        service.delete(id);
    }

    @GetMapping("/search/{id}")
    public ResponseEntity<Fattura> searchFattura (long id ){
        return new ResponseEntity<Fattura>(service.readById(id), HttpStatus.OK);
    }

    @PutMapping("/modify")
    public ResponseEntity<Fattura> modifyFattura (@RequestBody Fattura fattura, long id){
        service.update(id, fattura);
        return new ResponseEntity<Fattura>(fattura, HttpStatus.ACCEPTED);
    }

    
    
 
	
    
	@GetMapping("/filter/{importo}")
    public ResponseEntity<Page<Fattura>>findByImportoBetween(@RequestParam BigDecimal importomin,BigDecimal importomax) throws Exception{
        try {
            Page<Fattura> result = service.findByImportoBetween(importomin, importomax);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new Exception("Fattura non trovata");
        }
    }
    
    
    
    
	@GetMapping("/filter/{anno}")
    public ResponseEntity<Page<Fattura>>findByAnno(@RequestParam int anno) throws Exception{
        try {
            Page<Fattura> result = service.findByAnno(anno);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new Exception("Fattura non trovata");
        }
    }
    
    
    
	@GetMapping("/filter/{data}")
    public ResponseEntity<Page<Fattura>>findByData(@RequestParam Date data) throws Exception{
        try {
            Page<Fattura> result = service.findByData(data);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new Exception("Fattura non trovata");
        }
    }
    
    
    
	@GetMapping("/filter/{statoFattura}")
    public ResponseEntity<Page<Fattura>>findByStato(@RequestParam StatoFattura statoFattura) throws Exception{
        try {
            Page<Fattura> result = service.findByStatoFattura(statoFattura);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new Exception("Fattura non trovata");
        }
    }
    
    
    
	@GetMapping("/filter/{cliente}")
    public ResponseEntity<Page<Fattura>>findByCliente(@RequestParam Cliente cliente) throws Exception{
        try {
            Page<Fattura> result = service.findByCliente(cliente);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new Exception("Fattura non trovata");
        }
    }
    
    
    
}