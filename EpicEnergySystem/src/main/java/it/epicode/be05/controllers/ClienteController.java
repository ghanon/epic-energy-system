package it.epicode.be05.controllers;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.services.ClienteService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
	@Autowired
	ClienteService service;
	

	@PostMapping("/save")
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente){
		service.create(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteCliente (long id) {
		service.delete(id);		
	}
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Cliente> searchCliente (long id){
		return new ResponseEntity<Cliente>(service.readById(id),HttpStatus.ACCEPTED );
	}
	
	@PutMapping("/modify")
	public ResponseEntity<Cliente> modifyCliente (@RequestBody Cliente cliente, long id){
		service.update(id, cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.ACCEPTED);
	}
	
	
	
	
	@GetMapping("/search")
    public ResponseEntity<Page<Cliente>>findByName() throws Exception{

        try {
            Page<Cliente> result = service.findByRagioneSociale();
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	@GetMapping("/ordinefatturatoAnnuale")
    public ResponseEntity<Page<Cliente>>orderbyfatturatoAnnuale() throws Exception{

        try {
            Page<Cliente> result = service.orderByFatturatoAnnuale();
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	
	@GetMapping("/ordinedatainserimento")
    public ResponseEntity<Page<Cliente>>ordebydataInserimento() throws Exception{

        try {
            Page<Cliente> result = service.orderByDataInserimento();
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	
	@GetMapping("/ordinedataultimocontatto")
    public ResponseEntity<Page<Cliente>>ordebydataUltimoContatto() throws Exception{

        try {
            Page<Cliente> result = service.orderByDataUltimoContatto();
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	@GetMapping("/ordineprovincia")
    public ResponseEntity<Page<Cliente>>ordebyProvincia(@RequestParam LocalDate d) throws Exception{

        try {
            Page<Cliente> result = service.orderByProvincia();
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }	
	
	
	
	@GetMapping("/filter/{fatturatoAnnuale}")
    public ResponseEntity<Page<Cliente>>findbyfatturatoAnnuale(@RequestParam Long FattAnn) throws Exception{

        try {
            Page<Cliente> result = service.findByFatturatoAnnuale(FattAnn);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	@GetMapping("/filter/{dataInserimento}")
    public ResponseEntity<Page<Cliente>>findbydataInserimento(@RequestParam LocalDate d) throws Exception{

        try {
            Page<Cliente> result = service.findByDataInserimento(d);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	
	
	
	@GetMapping("/filter/{dataUltimoContatto}")
    public ResponseEntity<Page<Cliente>>findbydataUltimoContatto(@RequestParam LocalDate d) throws Exception{

        try {
            Page<Cliente> result = service.findByDataUltimoContatto(d);
            if(result.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            }else {
                return new ResponseEntity<>(result,HttpStatus.OK);
            }

        } catch (Exception e) {
            throw new Exception(" Cliente non trovato");
        }

    }
	

	
}
