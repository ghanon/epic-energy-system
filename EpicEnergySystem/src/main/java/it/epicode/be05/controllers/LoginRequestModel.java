package it.epicode.be05.controllers;

import lombok.Data;

@Data
public class LoginRequestModel {
	private String username;
	private String password;
}
