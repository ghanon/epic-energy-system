package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.ContattoCliente;
import it.epicode.be05.services.ContattoClienteService;

@RestController
@RequestMapping("/api/contattoCliente")
public class ContattoClienteController {

	@Autowired
	ContattoClienteService service;
	
	@PostMapping("/save")
	public ResponseEntity<ContattoCliente> saveContattoCliente(@RequestBody ContattoCliente contatto){
		service.create(contatto);
		return new ResponseEntity<ContattoCliente>(contatto, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteContattoCliente (long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<ContattoCliente> searchContattoCliente (long id ){
		return new ResponseEntity<ContattoCliente>(service.readById(id), HttpStatus.OK);
	}
	
	@PostMapping("/modify")
	public ResponseEntity<ContattoCliente> modifyContattoCliente (@RequestBody ContattoCliente contatto, long id){
		service.update(id, contatto);
		return new ResponseEntity<ContattoCliente>(contatto, HttpStatus.ACCEPTED);
	}
	
	
	
	
	
}
