package it.epicode.be05.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Comune;
import it.epicode.be05.runner.Anagrafica;
import it.epicode.be05.services.ComuneService;

@RestController
@RequestMapping("/api/comune")
public class ComuneController {

	@Autowired
	ComuneService service;
	
	
	@PostMapping("/save")
	public ResponseEntity<Comune> saveComune(@RequestBody Comune comune){
		service.create(comune);
		return new ResponseEntity<Comune>(comune, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteComune (long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Comune> searchComune (long id ){
		return new ResponseEntity<Comune>(service.readById(id), HttpStatus.OK);
	}
	
	@PostMapping("/modify")
	public ResponseEntity<Comune> modifyComune (@RequestBody Comune comune, long id){
		service.update(id, comune);
		return new ResponseEntity<Comune>(comune, HttpStatus.ACCEPTED);
	}
	
	@Autowired
	Anagrafica anagrafica;
	
	@PostMapping("/save/files")
	public ResponseEntity<?> readFile () {
		try {
	anagrafica.readProvincieComuni();
	return new ResponseEntity<>(HttpStatus.ACCEPTED);
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

}
