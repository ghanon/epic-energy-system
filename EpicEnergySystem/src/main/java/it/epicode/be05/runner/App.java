package it.epicode.be05.runner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Comune;
import it.epicode.be05.model.ContattoCliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.Indirizzo;
import it.epicode.be05.model.Provincia;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.model.TipoCliente;
import it.epicode.be05.services.ClienteService;
import it.epicode.be05.services.ComuneService;
import it.epicode.be05.services.ContattoClienteService;
import it.epicode.be05.services.FatturaService;
import it.epicode.be05.services.IndirizzoService;
import it.epicode.be05.services.ProvinciaService;
import it.epicode.be05.services.StatoFatturaService;

@Component
public class App implements CommandLineRunner{

	@Autowired
	ClienteService r1;
	@Autowired
	ComuneService r2;
	@Autowired
	ContattoClienteService r3;
	@Autowired
	FatturaService r4;
	@Autowired
	IndirizzoService r5;
	@Autowired
	ProvinciaService r6;
	@Autowired
	StatoFatturaService r7;
	@Autowired
	Anagrafica anagrafica;
	
	
	@Override
	public void run(String... args) throws Exception {
		anagrafica.readProvincieComuni();
		
		var a = StatoFattura.builder()
				.descrizione("NON PAGATA")
				.build();
		r7.create(a);
		
		var b = Provincia.builder()
				.nome("Treviso")
				.sigla("TV")
				.build();
		r6.create(b);
		
		var d = Comune.builder()
				.nome("Povegliano")
				.provincia(b)
				.build();
		r2.create(d);
		
		var c = Indirizzo.builder()
				.via("Via Treviso")
				.civico("20")
				.località("Santandrà")
				.cap(80055)
				.comune(d)
				.build();
		r5.create(c);
		
		var e = Fattura.builder()
				.anno(1800)
				.statoFattura(a)
				.numero(1)
				.importo(new BigDecimal(0.02))
				.data(new Date())
				.build();
		r4.create(e);
		
		var f = ContattoCliente.builder()
				.nomeContatto("Luca")
				.cognomeContatto("Zanatta")
				.emailContatto("lucazanatta90@gmail.com")
				.telefonoContatto("3496881864")
				.build();
		r3.create(f);
		
		
		List<Fattura> lista =new ArrayList<Fattura>();
		lista.add(e);
		
		var g = Cliente.builder()
				.ragioneSociale("ZanattaLuca SPA")
				.partitaIva("025466546954685")
				.email("info.zanattaluca.com")
				.dataInserimento(null)
				.dataUltimoContatto(null)
				.fatturatoAnnuale(310)
				.pec("zanattaluca@aruba.it")
				.telefono(0422770603)
				.contattoCliente(f)
				.indirizzoSedeLegale(c)
				.indirizzoSedeOperativa(null)
				.tipo(TipoCliente.SAS)
				.fattura(lista)
				.build();
		r1.create(g);	
	}
}
