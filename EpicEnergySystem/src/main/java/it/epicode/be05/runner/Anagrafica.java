package it.epicode.be05.runner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.epicode.be05.model.Comune;
import it.epicode.be05.model.Provincia;
import it.epicode.be05.repositories.ProvinciaRepository;
import it.epicode.be05.services.ComuneService;
import it.epicode.be05.services.ProvinciaService;
@Component
public class Anagrafica {

	@Autowired
	ProvinciaService service;

	@Autowired
	ComuneService serv;
	
	@Autowired
	ProvinciaRepository pr;

	public void readProvincieComuni() throws IOException{
		String riga1 ="";
		String riga2 = "";
		BufferedReader reader1 = null;
		BufferedReader reader2 = null;
		try {
			reader1 = new BufferedReader(new FileReader("src/main/resources/templates/comuni-italiani-1.csv"));
			reader2 = new BufferedReader(new FileReader("src/main/resources/templates/province-italiane-1.csv"));
			while ((riga2 = reader2.readLine()) != null) {
				String[] fields2 = riga2.split(";");
				var p = Provincia.builder().sigla(fields2[0]).nome(fields2[1]).build();
				service.create(p);
			}
			while ((riga1 = reader1.readLine()) != null) {
				
				String[] fields1 = riga1.split(";");
				var c = Comune.builder().nome(fields1[2]).provincia(pr.findByNome(fields1[3])).build();
				serv.create(c);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();

		}finally {
			try {
				reader1.close();
				reader2.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}



	}
}
