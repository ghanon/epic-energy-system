package it.epicode.be05.services.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.repositories.ClienteRepository;
import it.epicode.be05.services.ClienteService;
@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteRepository repo;
	
	@Override
	public void create(Cliente entity) {
		repo.save(entity);
	}

	@Override
	public Cliente readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public Page<Cliente> readAll(Pageable pageable) {

		return repo.findAll(pageable);
	}
	
	@Override
	public void update(long id, Cliente entity) {
	var p= repo.findById(id).orElseThrow();
	p.setContattoCliente(entity.getContattoCliente());
	p.setDataInserimento(entity.getDataInserimento());
	p.setDataUltimoContatto(entity.getDataUltimoContatto());
	p.setEmail(entity.getEmail());
	p.setFattura(entity.getFattura());
	p.setFatturatoAnnuale(entity.getFatturatoAnnuale());
	p.setIndirizzoSedeLegale(entity.getIndirizzoSedeLegale());
	p.setIndirizzoSedeOperativa(entity.getIndirizzoSedeOperativa());
	p.setPartitaIva(entity.getPartitaIva());
	p.setPec(entity.getPec());
	p.setRagioneSociale(entity.getRagioneSociale());
	p.setTelefono(entity.getTelefono());
	p.setTipo(entity.getTipo());
	repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}



	
	@Override
	public Page<Cliente> orderByFatturatoAnnuale() {
		Pageable p = PageRequest.of(0, 5);
		return repo.orderByFatturatoAnnuale(p);
	}
	


	@Override
	public Page<Cliente> orderByDataInserimento() {
		Pageable p = PageRequest.of(0, 5);
		return repo.orderByDataInserimento(p);
	}


	
	@Override
	public Page<Cliente> orderByDataUltimoContatto() {
		Pageable p = PageRequest.of(0, 5);
		return repo.orderByDataUltimoContatto(p);
	}


	
	@Override
	public Page<Cliente> orderByProvincia() {
		Pageable p = PageRequest.of(0, 5);
		return repo.orderByProvincia(p);
	}


	
	
	
	
	

	@Override
	public Page<Cliente> findByFatturatoAnnuale(Long FattAnn) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByFatturatoAnnuale(FattAnn,p);
    }

	@Override
	public Page<Cliente> findByDataInserimento(LocalDate d) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByDataInserimento(d,p);
	}

	@Override
	public Page<Cliente> findByDataUltimoContatto(LocalDate d) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByDataUltimoContatto(d,p);
	}

	@Override
	public Page<Cliente> findByRagioneSociale() {
	       Pageable p = PageRequest.of(0, 5, Sort.by("ragioneSociale"));
	        return repo.findAll(p);
	    }

}
