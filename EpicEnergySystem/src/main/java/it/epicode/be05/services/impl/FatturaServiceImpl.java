package it.epicode.be05.services.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.repositories.FatturaRepository;
import it.epicode.be05.services.FatturaService;
@Service
public class FatturaServiceImpl implements FatturaService{
	
	@Autowired
	FatturaRepository repo;

	@Override
	public void create(Fattura entity) {
		repo.save(entity);
		
	}

	@Override
	public Fattura readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Fattura entity) {
		var p= repo.findById(id).orElseThrow();
		p.setAnno(entity.getAnno());
		p.setData(entity.getData());
		p.setImporto(entity.getImporto());
		p.setNumero(entity.getNumero());
		p.setStatoFattura(entity.getStatoFattura());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Fattura> readAll(Pageable pageable) {
		return repo.findAll(pageable);
    }
	

	@Override
	public Page<Fattura> findByCliente(Cliente cliente) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByCliente(cliente,p);
    }
	

	@Override
	public Page<Fattura> findByStatoFattura(StatoFattura statoFattura) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByStatoFattura(statoFattura,p);
    }
	

	@Override
	public Page<Fattura> findByData(Date data) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByData(data,p);
    }

	@Override
	public Page<Fattura> findByAnno(int anno) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByAnno(anno,p);
    }

	@Override
	public Page<Fattura> findByImportoBetween(BigDecimal importoMin, BigDecimal importoMax) {
		Pageable p = PageRequest.of(0, 5);
		return repo.findByImportoBetween(importoMin, importoMax,p);
    }

}
