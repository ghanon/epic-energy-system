package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Comune;
import it.epicode.be05.repositories.ComuneRepository;
import it.epicode.be05.services.ComuneService;
@Service
public class ComuneServiceImpl implements ComuneService{
	
	@Autowired
	ComuneRepository repo;

	@Override
	public void create(Comune entity) {
		repo.save(entity);		
	}

	@Override
	public Comune readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Comune entity) {
		var p= repo.findById(id).orElseThrow();
		p.setNome(entity.getNome());
		p.setProvincia(entity.getProvincia());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Comune> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
