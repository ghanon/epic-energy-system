package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Provincia;
import it.epicode.be05.repositories.ProvinciaRepository;
import it.epicode.be05.services.ProvinciaService;
@Service
public class ProvinciaServiceImpl implements ProvinciaService{
	
	@Autowired
	ProvinciaRepository repo;

	@Override
	public void create(Provincia entity) {
		repo.save(entity);
	}

	@Override
	public Provincia readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Provincia entity) {
		var p= repo.findById(id).orElseThrow();
		p.setNome(entity.getNome());
		p.setSigla(entity.getSigla());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Provincia> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
