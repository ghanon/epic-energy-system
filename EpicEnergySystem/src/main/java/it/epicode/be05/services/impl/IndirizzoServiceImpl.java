package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Indirizzo;
import it.epicode.be05.repositories.IndirizzoRepository;
import it.epicode.be05.services.IndirizzoService;
@Service
public class IndirizzoServiceImpl implements IndirizzoService{
	
	@Autowired
	IndirizzoRepository repo;

	@Override
	public void create(Indirizzo entity) {
		repo.save(entity);
	}

	@Override
	public Indirizzo readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Indirizzo entity) {
		var p= repo.findById(id).orElseThrow();
		p.setCap(entity.getCap());
		p.setCivico(entity.getCivico());
		p.setComune(entity.getComune());
		p.setLocalità(entity.getLocalità());
		p.setVia(entity.getVia());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Indirizzo> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
