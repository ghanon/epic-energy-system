package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.repositories.StatoFatturaRepository;
import it.epicode.be05.services.StatoFatturaService;
@Service
public class StatoFatturaServiceImpl implements StatoFatturaService{
	
	@Autowired
	StatoFatturaRepository repo;

	@Override
	public void create(StatoFattura entity) {
		repo.save(entity);
	}

	@Override
	public StatoFattura readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, StatoFattura entity) {
		var p= repo.findById(id).orElseThrow();
		p.setDescrizione(entity.getDescrizione());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<StatoFattura> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}
}
