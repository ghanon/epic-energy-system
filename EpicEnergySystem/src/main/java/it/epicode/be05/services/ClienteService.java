package it.epicode.be05.services;

import java.time.LocalDate;

import org.springframework.data.domain.Page;

import it.epicode.be05.model.Cliente;

public interface ClienteService extends CrudService<Cliente> {
	
	

		//---METODI DI ORDINAMENTO

		Page<Cliente> orderByFatturatoAnnuale ();
		Page<Cliente> orderByDataInserimento();
		Page<Cliente> orderByDataUltimoContatto();
		Page<Cliente> orderByProvincia();
		
		//---METODI DI FILTRAGGIO	
		
		Page<Cliente> findByRagioneSociale ();
		Page<Cliente> findByFatturatoAnnuale (Long FattAnn);
		Page<Cliente> findByDataInserimento(LocalDate d);
		Page<Cliente> findByDataUltimoContatto(LocalDate d);
}
