package it.epicode.be05.services;

import it.epicode.be05.model.StatoFattura;

public interface StatoFatturaService extends CrudService<StatoFattura> {

}
