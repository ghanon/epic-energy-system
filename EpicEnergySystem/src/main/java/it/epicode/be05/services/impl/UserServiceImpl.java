package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.User;
import it.epicode.be05.repositories.UsersRepository;
import it.epicode.be05.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UsersRepository repo;

	@Override
	public void create(User entity) {
		repo.save(entity);
	}

	@Override
	public User readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public Page<User> readAll(Pageable p) {
		return repo.findAll(p);
	}

	@Override
	public void update(long id, User entity) {
		
		var user = repo.findById(id).orElseThrow();
		
		user.setEmail(entity.getEmail());
		user.setFullName(entity.getFullName());
		user.setUsername(entity.getUsername());
		
		repo.save(user);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public User getCurrentUser() {
		
		return repo.findAll(Pageable.unpaged()).get().findFirst().orElseThrow();
	}

}
