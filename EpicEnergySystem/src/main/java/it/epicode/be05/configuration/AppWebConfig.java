package it.epicode.be05.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import it.epicode.be05.configuration.AuthTokenFilter;
import it.epicode.be05.configuration.UnauthorizedUserJwtEntryPoint;

@SuppressWarnings("deprecation")
@Configuration 
@EnableWebSecurity 
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AppWebConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService detailsService;

	@Autowired
	private UnauthorizedUserJwtEntryPoint entryPoint;

	@Bean
	public AuthTokenFilter authenticationJwtToken() {
		return new AuthTokenFilter();
	}

	@Bean
	public PasswordEncoder getEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(detailsService) 
				.passwordEncoder(getEncoder()); 
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http 
			.cors().and().csrf().disable() 
			.exceptionHandling().authenticationEntryPoint(entryPoint) 
			.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() 
			.authorizeRequests().antMatchers("/**") 
			.permitAll().anyRequest().authenticated(); 
		
	
		http.addFilterBefore(authenticationJwtToken(), UsernamePasswordAuthenticationFilter.class);
	}
}
