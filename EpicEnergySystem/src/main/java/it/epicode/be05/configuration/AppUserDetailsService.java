package it.epicode.be05.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.epicode.be05.repositories.UsersRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	UsersRepository users;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		var user = users.findByUsername(username).orElseThrow();
		
		var appUser = AppUserDetails.build(user);
		return appUser;
	}

}
